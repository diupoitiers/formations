
## Codage de l'information

- [Prérequis - Binaire](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_1_1_binaire.pdf)
- [Codage des nombres flottants (et virgule fixe)](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_1_2_flottants.pdf)
- [Fichiers et formats usuels, compression et archivage](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_1_3_formats_compression.pdf)

## Algorithmique et programmation Python

- [Types structurés, p-uplets, tableaux et dictionnaires](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_2_1_types_structures.pdf)
- [Traitement de données en tables (recherche, tris, fusion)](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_2_2_traitement_donnees_tables.pdf)
- [Modularité, bibliothèques](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_2_3_modularite.pdf)
- [Spécifications, prototypage et tests](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_2_8_tests.pdf)
- [Diviser pour régner, algorithmes gloutons et programmation dynamique (Principes)](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_3_1_principes_algorithmique.pdf)
- [Rendu de monnaie](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_3_2_rendu_monnaie.pdf)
- [Tri par fusion](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_3_4_tri_fusion.pdf)
- [Problème du sac à dos](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_3_6_sacados.pdf)
- [Tri rapide](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_3_7_tri_rapide.pdf)
- [Programmation orientée objets](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_4_1_poo.pdf)
- [Attributs de classe et attributs d’instance](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_4_2_poo_attributs_classe.pdf)
- [Getters, setters et properties](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_4_3_poo_properties.pdf)
- [Structures de données abstraites](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_4_5_structures_abstraites.pdf)
- [Implémentation d’une file](https://formations-info-86.gitlab.io/diueil_algoprog/PDF/C_4_6_implementation_file.pdf)

## Réseau et cryptographie

[Réseaux, cryptographie - Survol](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_XX_survol.pdf)

### Réseau

- [Modèles en couches Osi et Tcp/Ip](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_00_modele_couches.pdf)
- [Gouvernance d’Internet](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_01_admin_internet.pdf)
- [Couche Réseau (Internet)](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_02_couche_reseau.pdf)
- [Couche Transport](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_03_couche_transport.pdf)
- [Couche d’accès au réseau](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_04_acces_reseau.pdf)
- [Couche Application](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_05_couche_application.pdf)
- [Protocoles de routage RIP et OSPF](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_20_rip_ospf.pdf)

### Crypto

- [Histoire de la cryptographie](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_06_crypto_histoire.pdf)
- [Cryptographie symétrique](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_07_crypto_sym.pdf)
- [Cryptographie asymétrique](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_08_crypto_asym.pdf)
- [Condensés](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_09_crypto_condenses.pdf)
- [Utilisation de la cryptographie](https://formations-info-86.gitlab.io/diueil_reseaux/PDF/C_4_10_crypto_utilisation.pdf)

## Algorithmes randomisés

- [Algorithmes randomisés](https://formations-info-86.gitlab.io/diueil_autres/RANDOMISES/PDF/C_5_1_algos_randomises.pdf)
- [Random… écueils](https://formations-info-86.gitlab.io/diueil_autres/RANDOMISES/PDF/C_5_3_random_ecueils.pdf)
- [Module random Python](https://formations-info-86.gitlab.io/diueil_autres/RANDOMISES/PDF/C_5_2_random_python.pdf)

## Bases de données

À noter : l'archive `Exercice requêtes SQL dans notebook + bdd + utils` contient tout le nécessaire pour concevoir des exercices
de bases de données sur une BDD de films (fournie) dans un notebook Jupyter.

- [Exercice de conception d'une base de données](https://formations-info-86.gitlab.io/diueil_autres/BDD/PDF/E_conception_banque.pdf) : 
- [Exercice sur les requêtes SQL](https://formations-info-86.gitlab.io/diueil_autres/BDD/PDF/E_requetes.pdf)
- [Exercice requêtes SQL dans notebook + bdd + utils](https://formations-info-86.gitlab.io/diueil_autres/BDD/notebook-requetes.tgz) et [vidéo : Comment exécuter des requêtes avec le notebook](https://videotheque.univ-poitiers.fr/vid/tutorequetesnb)
- [vidéo : Construire une requête par étapes sur la BDD des films](https://videotheque.univ-poitiers.fr/vid/constuctionreq/) : Si vous trouvez l'écriture 
      de requêtes difficile, cette vidéo dissèque un exemple et montre comment écrire une requête de manière incrémentale.
- Projet `Webfilms` : Ce travail est mis à disposition pour vous permettre de
  démarrer plus facilement des projets utilisant du web et des bases de données.
  Toujours en se basant sur la base de données de films, nous proposons un
  squelette de site Web utilisant Python,Flask (pour la partie serveur), Bulma
  (pour les CSS) et sqlite3 pour l'accès à la base. L'objectif est de faire une
  interface Web à la base de données.
  - Le projet (public) est disponible sur gitlab à cette adresse <https://gitlab.com/formations-info-86/projet_webfilms>.
  - Une vidéo explicative est disponible ici : [Vidéo](https://videotheque.univ-poitiers.fr/vid/webfilms/) (23 minutes)
