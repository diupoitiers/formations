# Utilisation de git

Reproduisez les manipulations évoquées dans la fiche de cours 

## Configuration et observation

1. Récupération du dépôt (zip)
2. Réglage config (nom, email, éditeur)
3. Affichage de l'historique sous forme détaillé et sous forme de graphe
4. Affichage des différences entre deux commits
5. Voir les dernières modifs `git show`

## Modification

1. Modifiez les 2 fichiers suivants `README.md` (ajout d’un énoncé) et `amicaux.py` (changement de 50 000 en 100 000)
2. Ajoutez le fichier `sucres.py`
3. Consulter le «status» et identifier l'état des modifications
4. Ajouter `README.md` et `sucres.py` à l'index
5. Reconsulter le status et relever les différences
6. Commiter les modifications
7. Afficher le graphe des commits (mode texte)

## Branches

Réinitialiser le répertoire (en dézippant de nouveau)

1. Basculer sur la branche `exo_amicaux`
2. Bonus : modifier le fichier `README.md`, essayer de revenir sur `main`... trouver comment **annuler** automatiquement la modification faite sur `README.md`
3. Modifier `amicaux.py` pour changer la borne de calcul à 100 000
4. Commiter cette modification
5. Observer le graphe
6. Rebasculer sur `main`
7. Tenter un merge de `exo_amicaux`
8. corriger manuellement et commiter
9. Observer le graphe

## Travailler avec un dépôt distant

Cette partie sera plutôt réalisée avec le dépôt de la banque d'exercice, sur framagit.
Pour travailler dans de bonnes conditions, créez une paire de clés, et configurez correctement framagit et
votre compte pour l'utiliser.


## Ressources sur GIT :new:

- [Learn Git banches](https://learngitbranching.js.org/?locale=fr_FR) : tutoriel visuel
- [Git Explorer](https://gitexplorer.com) propose d'explorer les commandes `git` à partir de menus déroulant décrivant des actions à mener.
- [Oh My Git!](https://ohmygit.org/) est un jeu permettant de se familiariser avec `git`.
- [Oh Shit Git](https://ohshitgit.com/fr) allez... courage... tout est réparable

## Outils Windows

- [Git for Windows](https://gitforwindows.org/)
- [Cmder](https://github.com/cmderdev/cmder)

