# Banque d'exercices / Dépôt `git`

**ATTENTION :
Le format des exercices a changé (30/10/2022). Le sujet initialement écrit dans le fichier nommé `sujet.md` 
doit maintenant se trouver dans le fichier nommé `index.md`. Les logiciels et scripts ont été mis à jour
en conséquence, et le texte qui suit doit être adapté (mais je ne le fais pas).**


- [Banque d’exercices / Dépôt git : fiche explicative](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_banque_exercices_git.pdf) [Archive](PDF/C_banque_exercice_git.pdf)
- [Dépôt framagit `banque_exercices`](https://framagit.org/info86/banque_exercices)

(ATTENTION : J'ai dû déplacer le dépôt de gitlab à framagit. Je peux expliquer pourquoi :)

La banque d'exercices est un dépôt qui permet :

- de centraliser et (un peu) catégoriser des exercices
- d'avoir à peu de frais une version en ligne propre accessible
- d'avoir à peu de frais la possibilité de créer des fichiers PDF avec plusieurs exercices dedans
- dans le cas des exercices avec correction automatique, d'utiliser les énoncés avec le système `pyjudge`

## Activité 1 

Inscription sur framagit, clonage du dépôt.

La banque d'exercices contient les énoncés, mais aussi les solutions. Le dépôt est donc privé. En conséquence, il
est nécessaire d'avoir un compte `framagit` pour accéder au dépôt et le cloner.

1. Créer un compte `framagit`, puis donnez votre pseudo pour être ajouté comme collborateur.
2. Vérifier l'accès au dépôt : <https://framagit.org/info86/banque_exercices>
3. Cloner le dépôt localement : `git clone <...>` en HTTPS ou SSH (voir le bouton bleu `Clone`)
4. Explorer les fichiers du dépôt : il y a des utilitaires, à la racine, puis un répertoire `banque`. Dans ce répertoire, il y a les exercices, un exercice par répertoire. Le nom de l'exercice est le nom du répertoire. Chaque répertoire contient au minimum le fichier `sujet.md`, généralement `solution.py`, et parfois `auto.py` ou encore `ressources.md`



## Activité 2

Utilisation des quelques scripts disponibles, disponibilité des sujets en ligne

### Liste des répertoires

```
cd banque_exercices
cd banque
ls
```

### Visualisation en ligne

Pour voir un exercice en ligne, utiliser : <https://info86.frama.io/banque_exercices/nom_repertoire_exo>.
Exemples : 

- <https://info86.frama.io/banque_exercices/code_mastermind/>
- <https://info86.frama.io/banque_exercices/leetspeak/>
- <https://info86.frama.io/banque_exercices/suite_padovan/>


### Titres des exercices

```
# Liste des exercices (on obtient le titre complet et le nom du rép)
./0_scripts/get_titles.sh 
```

Repérez l'exercice qui contient le mot `vallée` dans le titre. Repérez son nom court (nom du répertoire), construisez l'URL qui vous permet
de visualiser l'énoncé et vérifiez que ça fonctionne.

### Tags

`./0_scripts/tags.py` permet d'avoir la liste des tags, et combien d'exercices s'y rapportent. Il y a par exemple un tag `liste`.
`./0_scripts/tags.py liste` donne la liste de tous les exercices qui ont le tag `liste`.


### Création d'un PDF

Il y a un fichier `.gitignore` à la racine du dépôt. Dans ce fichier, vous pouvez voir : `/PDF_GEN`. Cela signifie que 
le répertoire `PDF_GEN` à la racine du dépôt ne sera pas indexé. Si ce répertoire n'existe pas, créez le.

Puis testez le script : `./0_scripts/build_student_sheet.sh` en créant un PDF de l'exercice dont le titre est :  *Bon ou mauvais parenthésage*
Pour créer des `pdf` avec ce script, il faut avoir le logiciel `pandoc` installé. Ce n'est pas forcément le cas sur votre machine.
Je peux vous montrer ce que ça donne lorsque le logiciel est disponible si vous demandez.

Le sujet sera placé dans le répertoire `PDF_GEN` qui est exclu du dépôt par `.gitignore` donc les PDF générés ne sont pas visibles dans le
dépôt en ligne.


### Création des fichiers html en local

Le script `0_scripts/build_gitlab_pages.sh` génère les pages HTML. Il nécessite aussi `pandoc`. C'est ce script qui est exécuté par
le système d'intégration continue de `framagit` pour générer les pages accessibles ensuite sur internet à l'adresse `https://info86.frama.io/banque_exercices/...`

Ce script est d'ailleurs lancé dès qu'il y a une modification dans la branche `main`. Donc les sujets en ligne sont toujours à jour.


## Activité 3

Contribution, ajout d'un exercice.

1. La branche sur laquelle vous êtes est `main` (faire `git satus` pour le voir). Avant de modifier quoi que ce soit, créez une nouvelle branche et basculez dessus : `git checkout -b votre_nom_formation_22`. Le nom n'est pas très important, et les branches seront finalement supprimées une fois les modifications intégrées, mais faites en sorte qu'on puisse vous identifier.
2. Maintenant que vous êtes sur la nouvelle branche, ajoutez un exercice. Vous pouvez créer les fichiers et dossiers manuellement, mais il y a un script qui fait ça (pour Linux ou MacOS, j'attends que l'un d'entre vous me file un `.bat` pour windows) : `0_scripts/template <monexo>`. Le nom de l'exercice doit permettre de l'identifier facilement, il ne doit pas contenir d'espace ni tout autre caractère qui poserait problème dans une URL.  Pour le moment, le système de nommage met les mots en minuscules, séparés par des `_`.
3. Aller dans le répertoire de l'exercice, et modifier `sujet.md`, renseignez les *tags*, mettez votre nom en auteur (on ne le voit pas ailleurs qu'en accédant au dépôt). Pensez à mettre quelques tests `assert ...` ce qui permet généralement de mieux comprendre ce qui est attendu (voir comment les autres exercices sont faits). Dans `ressources.md` vous pouvez ajouter des compléments, pour celui qui voudrait réutiliser cet exercice (sources du sujet, possibilités d'extensions etc.). 
4. **Important** : écrire la solution, dans le fichier `solution.py`. Ce fichier doit être exécutable, contenir des tests, au moins ceux de l'énoncé.
5. Une fois que l'exercice est OK, il faut valider les changement dans votre branche. Pour ça :
- `git status` permet de voir où vous en êtes
- `git add <fichier>` permet d'ajouter un fichier modifié à la zone de *staging* (modifications en attente de validation)
- `git commit` valide les changements dans la zone de *staging*. Mettez un message de commit court et si possible informatif (mais inutile de redonner le nom de l'exo).
6. À présent, il faut pousser ces modifications (toute votre branche) vers le serveur :
7. `git push` (*git* va probablement vous indiquer de donner quelques informations en plus... faites ce qu'il dit)
8. À présent, rendez-vous sur le site, et utilisez le nouveau bouton *Merge Request*  pour demander à ce que vos modifs soient intégrées au dépôt.
9. Patientez (votre demande doit être acceptée)... et voilà, vous avez collaboré !

## Activité 4

Utilisation de `pyjudge`

Certains exercices apparaissent avec un emoji :computer: sous le titre lorsqu'on consulte la version Web.
Cela signifie que l'exercice possède un fichier `auto.py` (et non `auto_.py`), ce qui permet a priori d'avoir 
une évaluation automatique de l'exercice.

Choisissez un exercice qui a un tel logo, par exemple `parenthesage`.

1. Rendez-vous sur une instance de pyjudge : <https://deptinfo-ensip.univ-poitiers.fr/pyjudge/>
2. Installez le client comme demandé sur la page en question
3. Complétez le code donné plus bas, avec le token de connexion suivant : `0:/sandbox/toto:241022.z0omw7XXyNf72sen6WEFiPXvl0I`
4. Faites l'exercice !

```python
from pyjudge_client import PyJudge # C'est le module que vous avez installé

pj = PyJudge("https://deptinfo-ensip.univ-poitiers.fr/pyjudge/") # Pourrait fonctionner avec une autre instance (la votre ?)
pj.auth('0:/sandbox/toto:241022.z0omw7XXyNf72sen6WEFiPXvl0I') # Token de connexion (explications à la demande... :) )

def ma_fonction(...):
    ...

res = pj.feedback(ma_fonction, "0:parenthesage") # Le 0: est le préfixe de la banque d'exercice à laquelle vous avez accès
```

Si vous êtes dans un notebook, vous pouvez utiliser `pj.feedback(ma_fonction, "0:parenthesage", mode="html")` pour avoir une sortie plus jolie.

## Activité 5

Faire une partie de [Oh my git!](https://ohmygit.org/) ?

## D'autres outils à venir

Il y a d'autres outils disponibles, généralement pour Linux, et parfois accessibles dans le dépôt `pyjudge` 
(... pas forcément pratique pour tout le monde).

Réalisation d'une fiche d'exercices contenant plusieurs exercices : 

```
./0_scripts/manipulation.py  compose --title "Fiche 1" -o fiche1.md code_mastermind leetspeak suite_padovan 
pandoc-pdf fiche1.md
okular fiche1.pdf
```

Liste des exercices, et des exercices ayant un certain tag : 

```
./0_scripts/manipulation.py get_exos
./0_scripts/manipulation.py get-tag liste
```

Construction d'un notebook contenant certains exercices  :

```
# Après démarrage du serveur pyjudge
./0_scripts/build_notebook.py  'Notebook exercice numéro 1' "O" 0:nombres_chanceux 0:somme_carres > notebook.py
jupyter notebook notebook.py
```
